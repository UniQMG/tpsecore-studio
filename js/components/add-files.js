export default {
  template: `
    <div class="add-files">
      <input ref="file" type="file" @change="addFile" multiple style="display: none"></input>
      <button @click="$refs.file.click()">Add files</button>

      <button @click="addDirectory">Add folder (chrome only)</button>

      <div class="checkbox-group">
        <input type="checkbox" name="autorefresh" v-model="autoRefresh.value"/>
        <label for="autorefresh">Auto-reload</label>
      </div>
    </div>
  `,
  props: ['worker', 'autoRefresh'],
  methods: {
    async addFile() {
      console.log("add", this.$refs.file.files);
      for (let file of this.$refs.file.files)
        await this.worker.addFile(file);
      this.$refs.file.value = '';
      await this.worker.doImport();
    },
    async addDirectory() {
      let directory = await showDirectoryPicker({ id: 0, startIn: 'documents' });
      await this.worker.addDirectory(directory);
      await this.worker.doImport();
    }
  }
}
