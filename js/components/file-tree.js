export default {
  template: `
    <div class="file-tree">
      <div class="file" v-for="(file, i) of worker.files">
        <button
          class="remove"
          @click="worker.files.splice(i, 1); worker.doImport();"
        >✘</button>
        <span class="icon">🗎</span>
        {{ file.path }}
      </div>

      <div class="directory" v-for="(dir, i) of worker.directories">
        <button
          class="remove"
          @click="worker.directories.splice(i, 1); worker.doImport();"
        >✘</button>
        <span class="icon">🗀</span>
        {{ dir.directory.name }}
        <div class="file" v-for="file of dir.files">
          <span class="icon">🗎</span>
          {{ file.path }}
        </div>
      </div>
    </div>
  `,
  props: ['worker']
}
