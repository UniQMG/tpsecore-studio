import { ALL_BOARD_OPTIONS } from '../default-options.js';

export default {
  template: `
    <div class="options">
      <div style="display: flex; gap: 2px; flex-wrap: wrap;">
        <button class="busyable" @click="exportTPSE" :disabled="exporting">Export TPSE</button><!--
        --><button class="busyable" @click="refresh" :disabled="worker.lock.locked">Refresh preview</button><!--
        --><button class="busyable" @click="reload" :disabled="worker.lock.locked">Reload folder<template v-if="worker.directories.length != 1">s</template></button>
      </div>

      <div class="section-header">
        <div>Preview options</div>
        <button @click="reset">Reset all</button>
      </div>

      <div class="control-group">
        <label for="map" style="font-weight: bold">Map</label>
        <input type="text" v-model="map" name="map">
      </div>
      <div>
        (use <a href="https://you.have.fail/at/tetrio/connected-map-editor/">this tool</a> to edit)
      </div>

      <div class="checkbox-group">
        <input type="checkbox" v-model="worker.options.debug_grid" name="debug_grid">
        <label for="debug_grid">Debug grid</label>
      </div>

      <div class="control-group">
        <label for="skyline" style="font-weight: bold">Skyline</label>
        <input type="spinner" v-model.number="worker.options.skyline" name="skyline" min="1" max="40">
      </div>

      <div class="section-header">
        <div>Board elements</div>
        <button @click="setAllBoardElements(true)">Enable all</button>
        <button @click="setAllBoardElements(false)">Disable all</button>
      </div>
      <div class="checkbox-group" style="margin: 2px 0px" v-for="option of ALL_BOARD_OPTIONS">
        <input type="checkbox" :checked="hasBoardElement(option)" @click="toggleBoardElement(option)" :name="option"/>
        <label :for="option">{{ option }}</label>
      </div>
    </div>
  `,
  props: ['worker'],
  data: () => ({ ALL_BOARD_OPTIONS, exporting: false, invalid_map: null }),
  watch: {
    'worker.options': {
      deep: true,
      handler() {
        this.worker.dirty = true;
        console.log('options changed');
      }
    }
  },
  computed: {
    map: {
      get() {
        return this.invalid_map || JSON.stringify(this.worker.options.frames);
      },
      set(val) {
        try {
          this.worker.options.frames = JSON.parse(val);
          this.invalid_map = null;
        } catch(ex) {
          this.invalid_map = val;
        }
      }
    }
  },
  methods: {
    async exportTPSE() {
      this.exporting = true;
      let tpse = await this.worker.exportTPSE();
      let el = document.createElement('a');
      el.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(tpse));
      el.setAttribute('download', 'tetrio-plus-settings-export.tpse');
      el.style.display = 'none';
      document.body.appendChild(el);
      el.click();
      el.remove();
      this.exporting = false;
    },
    async refresh() {
      await this.worker.doImport();
    },
    async reload() {
      await this.worker.refreshDirectories();
      if (this.worker.dirty)
        await this.worker.doImport();
    },
    reset() {
      this.worker.resetOptions()
      this.invalid_map = null;
    },
    setAllBoardElements(val) {
      this.worker.options.board_elements = val ? [...ALL_BOARD_OPTIONS] : [];
    },
    toggleBoardElement(el) {
      let elements = this.worker.options.board_elements;
      let idx = elements.indexOf(el);
      if (idx == -1) elements.push(el);
      else elements.splice(idx, 1);
    },
    hasBoardElement(el) {
      return this.worker.options.board_elements.includes(el);
    }
  }
}
