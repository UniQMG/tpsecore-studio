export default {
  template: `<div class="render-frame"><img :src="url" :style="style"/></div>`,
  props: ['frames', 'frame'],
  data: () => ({ url: null }),
  watch: {
    frame: {
      immediate: true,
      handler(frame) {
        let blob = new Blob([new Uint8Array(frame.image)], {type: "image/png"});
        this.setURL(URL.createObjectURL(blob));
      }
    }
  },
  beforeDestroy() {
    this.setURL(null);
  },
  computed: {
    style() {
      let { frames, frame } = this;
      let min_x = Math.min(...frames.map(el => el.min_x));
      let min_y = Math.min(...frames.map(el => el.min_y));
      let max_x = Math.max(...frames.map(el => el.max_x));
      let max_y = Math.max(...frames.map(el => el.max_y));
      return {
        marginTop:    Math.abs(min_y - frame.min_y) + 'px',
        marginRight:  Math.abs(max_x - frame.max_x) + 'px',
        marginBottom: Math.abs(max_y - frame.max_y) + 'px',
        marginLeft:   Math.abs(min_x - frame.min_x) + 'px'
      }
    }
  },
  methods: {
    setURL(url) {
      if (this.url) URL.revokeObjectURL(this.url);
      this.url = url;
    }
  }
}
