import defaultOptions from './default-options.js';

export default class WorkerManager {
  static #fileIdNonce = 0;

  constructor() {
    this.lock = new Lock();
    this.worker = new Worker('js/worker.js', { type: 'module' });

    this.messageHooks = [event => this.#handleMessage(event)];
    this.worker.addEventListener('message', event => {
      for (let i = this.messageHooks.length-1; i >= 0; i--)
        if (this.messageHooks[i](event.data) === false)
          this.messageHooks.splice(i, 1);
    });

    this.lastRender = null;
    this.lastRenderDate = Date.now();

    this.files = [];
    this.directories = [];
    this.dirty = false;

    this.logs = [];

    this.options = defaultOptions();
  }

  resetOptions() {
    this.options = defaultOptions();
    this.dirty = true;
  }

  onMessage(handler) {
    this.messageHooks.push(handler);
  }

  async #handleMessage(data) {
    switch (data.type) {
      case 'log':
        if (data.level == 'TRACE' || data.level == 'DEBUG') break;
        this.logs.push({ date: new Date(), ...data });
        break;

      case 'render':
        this.lastRender = data.render;
        break;

      case 'readfile':
        try {
          console.log(data.file, this.allFilesById(), this.lock.locked);
          let content = await this.allFilesById()[data.file].read();
          this.worker.postMessage({ type: 'response', nonce: data.nonce, data: content });
        } catch(ex) {
          this.worker.postMessage({ type: 'response', nonce: data.nonce, error: ex });
        }
        break;
    }
  }

  allFiles() {
    return [
      ...this.files,
      ...this.directories.flatMap(({ files }) => Object.values(files))
    ];
  }
  allFilesById() {
    return Object.fromEntries(this.allFiles().map(file => [file.id, file]));
  }

  async addFile(file) {
    await this.lock.acquire();
    this.files.push({
      id: ++WorkerManager.#fileIdNonce,
      path: file.name,
      name: file.name,
      modified: Date.now(),
      read: async () => new Uint8Array(await file.arrayBuffer())
    });
    this.dirty = true;
    this.lock.release();
  }

  async addDirectory(directory) {
    await this.lock.acquire();
    let files = await this.#loadDirectory(directory);
    this.directories.push({ directory, files });
    this.dirty = true;
    this.lock.release();
  }

  async refreshDirectories() {
    await this.lock.acquire();
    for (let i = 0; i < this.directories.length; i++) {
      let oldFiles = this.directories[i].files;
      let files = await this.#loadDirectory(this.directories[i].directory);
      this.directories[i].files = files;

      if (files.some(file => file.modified > this.lastRenderDate)) this.dirty = true;
      if (files.length != oldFiles.length) this.dirty = true;
    }
    this.lock.release();
  }

  async #loadDirectory(dir, files=[], path="/") {
    for await (let [name, entry] of dir.entries()) {
      if (entry instanceof FileSystemDirectoryHandle) {
        // console.log('dir', name, entry);
        await this.#loadDirectory(entry, files, `${path}${name}/`);
      }
      if (entry instanceof FileSystemFileHandle) {
        let file = await entry.getFile();
        // console.log('file', name, entry, file);
        files.push({
          id: ++WorkerManager.#fileIdNonce,
          path: path + name,
          name: name,
          modified: file.lastModified,
          read: async () => new Uint8Array(await file.arrayBuffer())
        });
      }
    }
    return files;
  }

  async doImport() {
    await this.lock.acquire();
    this.lastRenderDate = Date.now();
    this.dirty = false;
    this.logs.length = 0;

    let importDone = new Promise(res => {
      // console.log('awaiting render finish');
      this.onMessage(data => {
        console.log('event', data);
        if (data.type == 'status' && data.statusCode == 'import') {
          // console.log('achieved status import', data);
          this.onMessage(data => {
            if (data.type == 'status' && data.statusCode != 'import') {
              // console.log('achieved status not importing', data);
              res();
              return false;
            }
          });
          return false;
        }
      });
    });

    this.worker.postMessage({
      type: 'renderPreview',
      files: this.allFiles().map(({ id, path }) => ({ id, path })),
      options: this.options
    });

    await importDone;

    this.lock.release();
  }

  async exportTPSE() {
    await this.lock.acquire();
    this.logs.length = 0;
    let tpsePromise = new Promise(res => {
      this.onMessage(data => {
        if (data.type == 'exportTPSEResult') {
          res(data.tpse);
          return false;
        }
      })
    });
    this.worker.postMessage({
      type: 'exportTPSE',
      files: this.allFiles().map(({ id, path }) => ({ id, path }))
    });
    let tpse = await tpsePromise;
    this.lock.release();
    return tpse;
  }
}

class Lock {
  constructor() {
    this.locked = false;
    this.onUnlock = [];
  }
  async acquire() {
    while (this.locked)
      await new Promise(res => this.onUnlock.push(res));
    this.locked = true;
    // console.log('locked');
  }
  release() {
    // console.log('unlocked');
    this.locked = false;
    for (let wakeup of this.onUnlock.splice(0))
      wakeup();
  }
}
