import init, * as tpsecore from './lib/tpsecore.js';

status('init', 'loading tpsecore...');
await new Promise(res => setTimeout(res, 1000));
await init();
status('idle', 'ready');

let tasks = [];
let promises = new Map();

onmessage = async function(msg) {
  console.log('Worker>', msg.data);
  switch (msg.data.type) {
    case 'renderPreview': {
      tasks.push(() => renderPreview(msg.data.files, msg.data.options));
      break;
    }
    case 'exportTPSE': {
      tasks.push(() => exportTPSE(msg.data.files));
      break;
    }
    case 'response': {
      let { resolve, reject } = promises.get(msg.data.nonce);
      promises.delete(msg.data.nonce);
      if (msg.data.error) reject(msg.data.error);
      if (msg.data.data) resolve(msg.data.data);
      break;
    }
  }
};

(async function mainLoop() {
  while (true) {
    await new Promise(res => setTimeout(res, 10));
    if (tasks.length == 0) continue;
    for (let task of tasks.splice(0)) {
      try {
        await task();
        status('idle', 'ready');
      } catch(ex) {
        status('idle', 'error: ' + ex);
      }
    }
  }
})();

function status(statusCode, statusMessage) {
  postMessage({ type: 'status', statusCode, statusMessage });
}

let nonces = 0;
function call(type, data) {
  return new Promise((resolve, reject) => {
    let nonce = ++nonces;
    promises.set(nonce, { resolve, reject });
    postMessage({ type, nonce, ...data });
  });
}

async function doImport(files) {
  status('import', 'running import');

  let tpse = tpsecore.create_tpse();
  //console.log('files', files);
  for (let { id, path } of files) {
    status('import', `running import: ${path}`);
    let filename = path.split('/').slice(-1)[0];
    let content = await call('readfile', { file: id });
    //console.log('read result', { id, path, content });
    tpsecore.import_file(tpse, { type: 'automatic' }, filename, content, (lvl, msg) => {
      postMessage({ type: 'log', level: lvl, message: msg });
    });
  }
  return tpse;
}

async function renderPreview(files, opts) {
  let tpse = await doImport(files);
  status('import', 'rendering');
  let [frames, audio] = tpsecore.render_video(tpse, opts);
  postMessage({ type: 'render', render: { frames, audio } });
  tpsecore.drop_tpse(tpse);
}

async function exportTPSE(files) {
  let tpseId = await doImport(files);
  status('import', 'generating TPSE');
  let tpse = tpsecore.export_tpse(tpseId);
  postMessage({ type: 'exportTPSEResult', tpse });
  tpsecore.drop_tpse(tpseId);
}
