import WorkerManager from './worker-manager.js';

import AddFiles from './components/add-files.js';
import FileTree from './components/file-tree.js';
import Options from './components/options.js';
import RenderFrame from './components/render-frame.js';

let app = new Vue({
  template: `
    <div class="app">
      <div class="header">
        TPSECORE STUDIO
      </div>

      <div class="tabs">
        <button class="tab" @click="tab = 'preview'" :class="{ active: tab == 'preview' }">Preview</button>
        <button class="tab" @click="tab = 'log'" :class="{ active: tab == 'log' }">Import log</button>
      </div>

      <add-files :worker="worker" :auto-refresh="autoRefresh" />

      <file-tree :worker="worker" />

      <options :worker="worker" />

      <div class="render" v-show="tab == 'preview'">
        <render-frame v-if="lastRender" :frames="lastRender.frames" :frame="lastRender.frames[0]"/>
        <div v-else>No preview yet</div>
      </div>
      <div class="logs" v-show="tab == 'log'">
        <div v-if="worker.logs.length == 0">
          No logs yet
        </div>
        <div class="log" v-for="log of worker.logs">
          [{{ log.date.toISOString() }} {{ log.level }}] {{ log.message }}
        </div>
      </div>

      <div class="footer">
        <div class="status-led" :class="statusCode"></div>
        <div class="status-code">{{ statusCode.toUpperCase() | CAPITALIZE }}</div>
        <div class="seperator"></div>
        <div class="status-text">{{ statusMessage | Capitalize }}</div>
      </div>
    </div>
  `,
  components: { AddFiles, FileTree, Options, RenderFrame },
  data: () => ({
    tab: 'preview',
    worker: new WorkerManager(),
    statusCode: 'init',
    statusMessage: 'waiting on worker...',
    autoRefresh: { value: false }
  }),
  filters: {
    CAPITALIZE(val) { return val.toUpperCase() },
    Capitalize(val) { return val[0].toUpperCase() + val.slice(1) }
  },
  mounted() {
    this.worker.onMessage(event => this.message(event));
    this.refreshLoop();
  },
  computed: {
    lastRender() { return this.worker.lastRender }
  },
  methods: {
    async refreshLoop() {
      while (true) {
        await new Promise(res => setTimeout(res, 100));
        if (!this.autoRefresh.value) continue;
        await this.worker.refreshDirectories();
        if (this.worker.dirty)
          await this.worker.doImport();
      }
    },
    async message(data) {
      // console.log('App>', data);
      switch (data.type) {
        case 'status':
          this.statusCode = event.data.statusCode;
          this.statusMessage = event.data.statusMessage;
          break;
      }
    }
  }
});
window.app = app;
app.$mount('#app');
