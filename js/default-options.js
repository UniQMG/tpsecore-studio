export const ALL_BOARD_OPTIONS = `background,mini_grid_border,name_tag_background,name_tag_background_on_fire,danger_line,danger_glow,board_grid_borders_inner_bottom,garbage_bar,progress_bar,stock,garbage,progress,garbage_cap,warning,target,pending_garbage,mega_background,mega_foreground,hold,next,replay`.split(',');

export default () => ({
  frames: [[[null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null],[["z",4],["z",3],null,["topout",38],["topout",19],null,["i",4],["i",5],["i",5],["i",1]],[["t",2],["z",12],["z",1],["topout",76],["topout",137],["ghost",2],["j",2],["o",38],["o",19],["l",2]],[["t",14],["t",1],["s",6],["s",1],["ghost",4],["ghost",11],["j",10],["o",76],["o",137],["l",10]],[["t",8],["s",4],["s",9],null,null,["ghost",8],["j",12],["j",1],["l",4],["l",9]],[["garbage",4],["garbage",5],["garbage",5],["garbage",1],["darkgarbage",0],["garbage",4],["garbage",5],["garbage",5],["garbage",5],["garbage",1]]]],
  frame_duration: 0,
  frame_rate: 24,
  board_elements: "background,mini_grid_border,name_tag_background,name_tag_background_on_fire,danger_line,danger_glow,board_grid_borders_inner_bottom,garbage_bar,progress_bar,stock,garbage,progress,garbage_cap,warning,target,pending_garbage,mega_background,mega_foreground,hold,next,replay".split(','),
  debug_grid: false,
  skyline: 20,
  block_size: 40,
  sound_effects: []
});
